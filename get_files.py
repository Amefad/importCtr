#script import CTR
#Amedeo Fadini Tepco srl 
#16 febbraio 2015 
#tepco@tepco.it
#
##############################

import os
import string
import zipfile
import shutil
import sys
from osgeo import gdal
#Enable GDAL/ORG exceptions
gdal.UseExceptions()
#from os import walk

mypath = os.getcwd() + '/ctr'
db_name = 'prova_ctr.sqlite'

#nothing to change below?

#def crea_db(percorso, nomedb):
#crea database spatialite in base a percorso e nome
#ogr2ogr -f SQLite -dsco SPATIALITE=YES percorso first.shp -nln the_table -lco SPATIAL_INDEX=NO

def decomprimi(zippath):
	print "decomprimi" + zippath
	#decomprime ciascun file e lo salva in tmp
	if not(os.path.exists('tmp')):
		os.mkdir('tmp')
		for (path, dirs, files) in os.walk(zippath):
			for name in files:
				if (''.join(name)[-3:] == 'zip'):
					zip = os.path.join(path, name)
					with zipfile.ZipFile(zip, "r") as z:
						z.extractall("tmp/")
	cerca_shp('tmp')
	#shutil.rmtree('tmp')			

def importa_shp(filename, database):
	#importa lo shape "filename" come una tabella del database spatialite
	#debug 
	print 'shape ' + filename + ' nel db ' + database

def cerca_shp(init_path):
	#crea la lista f con tutti i nomi dei file zip
	f = []
	for (path, dirs, files) in os.walk(init_path):
		for name in files:
			if (''.join(name)[-3:] == 'shp'):
				shape = os.path.join(path, name)
				importa_shp(shape, db_name)

#decomprimi(mypath)
#cerca_shp(mypath)
#print "finito"