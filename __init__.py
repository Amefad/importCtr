# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ImportCtr
                                 A QGIS plugin
 Script per l'importazione della CTRN Veneto e vestizione
                             -------------------
        begin                : 2015-03-31
        copyright            : (C) 2015 by Amedeo Fadini Tepco srl
        email                : tepco@tepco.it
        git sha              : $Format:%H$
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
 This script initializes the plugin, making it known to QGIS.
"""


# noinspection PyPep8Naming
def classFactory(iface):  # pylint: disable=invalid-name
    """Load ImportCtr class from file ImportCtr.

    :param iface: A QGIS interface instance.
    :type iface: QgsInterface
    """
    #
    from .ctr_import import ImportCtr
    return ImportCtr(iface)
