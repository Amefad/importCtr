Attenzione!!
Plugin non ancora funzionante!

Questo progetto riguarda la creazione di un plugin per l'importazione  e la vestizione in Qgis dei file della ctr del veneto.

http://idt.regione.veneto.it/app/metacatalog/

L'idea è quella di estrarre i file SHP dai rispettivi archivi zippati e inserirli in un database spatialite.

Il template del database è nella cartella /data del plugin e contiene la tabella 
con gli stili da assegnare per la CTRN scala 1 a 5000
Gli stili sono rilasciati con licenza CC-BY Tepco srl

Il file principale è il file ctr_import_dialog.py

cosa funziona:
create_db()
crea il database nella posizione desiderata a partire dal template in ./data
decomprimi()
estrae correttamente i file nella cartella /tmp del plugin


importa_shp() 
lancia il comando ogr2ogr tramite subprocess per l'importazione di ogni shapefile in ./tmp nel nuovo database
(la variabile STARTUPINFO() serve per non mostrare il prompt dei comandi in windows)
In alcuni casi il database viene creato correttamente in altri no


Cosa non funziona:
- se il plugin è abilitato da errore all'avvio di Qgis, mentre non crea problemi se caricato con il plugin reloader
- la funzione carica_db() è ancora da scrivere, per il momento non funziona

Ogni consiglio è bene accetto

21/04/2015

amefad
fame@libero.it