# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'ctr_import_dialog_base.ui'
#
# Created: Thu Apr  9 10:23:30 2015
#      by: PyQt4 UI code generator 4.9.3
#
# WARNING! All changes made in this file will be lost!

from PyQt4 import QtCore, QtGui

try:
    _fromUtf8 = QtCore.QString.fromUtf8
except AttributeError:
    _fromUtf8 = lambda s: s

class Ui_ImportCtrDialogBase(object):
    def setupUi(self, ImportCtrDialogBase):
        ImportCtrDialogBase.setObjectName(_fromUtf8("ImportCtrDialogBase"))
        ImportCtrDialogBase.resize(409, 329)
        self.verticalLayoutWidget = QtGui.QWidget(ImportCtrDialogBase)
        self.verticalLayoutWidget.setGeometry(QtCore.QRect(10, 10, 381, 301))
        self.verticalLayoutWidget.setObjectName(_fromUtf8("verticalLayoutWidget"))
        self.verticalLayout = QtGui.QVBoxLayout(self.verticalLayoutWidget)
        self.verticalLayout.setMargin(0)
        self.verticalLayout.setObjectName(_fromUtf8("verticalLayout"))
        self.label = QtGui.QLabel(self.verticalLayoutWidget)
        self.label.setObjectName(_fromUtf8("label"))
        self.verticalLayout.addWidget(self.label)
        self.horizontalLayout = QtGui.QHBoxLayout()
        self.horizontalLayout.setObjectName(_fromUtf8("horizontalLayout"))
        self.src_ledit = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.src_ledit.setObjectName(_fromUtf8("src_ledit"))
        self.horizontalLayout.addWidget(self.src_ledit)
        self.pushButtonSrc = QtGui.QPushButton(self.verticalLayoutWidget)
        self.pushButtonSrc.setObjectName(_fromUtf8("pushButtonSrc"))
        self.horizontalLayout.addWidget(self.pushButtonSrc)
        self.verticalLayout.addLayout(self.horizontalLayout)
        self.label_2 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_2.setObjectName(_fromUtf8("label_2"))
        self.verticalLayout.addWidget(self.label_2)
        self.horizontalLayout_2 = QtGui.QHBoxLayout()
        self.horizontalLayout_2.setObjectName(_fromUtf8("horizontalLayout_2"))
        self.dst_ledit = QtGui.QLineEdit(self.verticalLayoutWidget)
        self.dst_ledit.setObjectName(_fromUtf8("dst_ledit"))
        self.horizontalLayout_2.addWidget(self.dst_ledit)
        self.pushButtonDst = QtGui.QPushButton(self.verticalLayoutWidget)
        self.pushButtonDst.setObjectName(_fromUtf8("pushButtonDst"))
        self.horizontalLayout_2.addWidget(self.pushButtonDst)
        self.verticalLayout.addLayout(self.horizontalLayout_2)
        self.chk_view = QtGui.QCheckBox(self.verticalLayoutWidget)
        self.chk_view.setObjectName(_fromUtf8("chk_view"))
        self.verticalLayout.addWidget(self.chk_view)
        self.progressBar = QtGui.QProgressBar(self.verticalLayoutWidget)
        self.progressBar.setProperty("value", 20)
        self.progressBar.setObjectName(_fromUtf8("progressBar"))
        self.verticalLayout.addWidget(self.progressBar)
        self.progress_label = QtGui.QLabel(self.verticalLayoutWidget)
        self.progress_label.setObjectName(_fromUtf8("progress_label"))
        self.verticalLayout.addWidget(self.progress_label)
        self.horizontalLayout_3 = QtGui.QHBoxLayout()
        self.horizontalLayout_3.setObjectName(_fromUtf8("horizontalLayout_3"))
        self.label_3 = QtGui.QLabel(self.verticalLayoutWidget)
        self.label_3.setText(_fromUtf8(""))
        self.label_3.setPixmap(QtGui.QPixmap(_fromUtf8(":/plugins/ImportCtr/tepco_logo.png")))
        self.label_3.setObjectName(_fromUtf8("label_3"))
        self.horizontalLayout_3.addWidget(self.label_3)
        self.button_box = QtGui.QDialogButtonBox(self.verticalLayoutWidget)
        self.button_box.setOrientation(QtCore.Qt.Horizontal)
        self.button_box.setStandardButtons(QtGui.QDialogButtonBox.Close|QtGui.QDialogButtonBox.Ok)
        self.button_box.setCenterButtons(False)
        self.button_box.setObjectName(_fromUtf8("button_box"))
        self.horizontalLayout_3.addWidget(self.button_box)
        self.verticalLayout.addLayout(self.horizontalLayout_3)

        self.retranslateUi(ImportCtrDialogBase)
        QtCore.QObject.connect(self.button_box, QtCore.SIGNAL(_fromUtf8("accepted()")), ImportCtrDialogBase.accept)
        QtCore.QObject.connect(self.button_box, QtCore.SIGNAL(_fromUtf8("rejected()")), ImportCtrDialogBase.reject)
        QtCore.QMetaObject.connectSlotsByName(ImportCtrDialogBase)

    def retranslateUi(self, ImportCtrDialogBase):
        ImportCtrDialogBase.setWindowTitle(QtGui.QApplication.translate("ImportCtrDialogBase", "Crea Db Ctr Veneto", None, QtGui.QApplication.UnicodeUTF8))
        self.label.setText(QtGui.QApplication.translate("ImportCtrDialogBase", "Cartella con i file della ctr", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonSrc.setText(QtGui.QApplication.translate("ImportCtrDialogBase", "Sfoglia...", None, QtGui.QApplication.UnicodeUTF8))
        self.label_2.setText(QtGui.QApplication.translate("ImportCtrDialogBase", "Database di destinazione", None, QtGui.QApplication.UnicodeUTF8))
        self.pushButtonDst.setText(QtGui.QApplication.translate("ImportCtrDialogBase", "Sfoglia...", None, QtGui.QApplication.UnicodeUTF8))
        self.chk_view.setText(QtGui.QApplication.translate("ImportCtrDialogBase", "Carica in Qgis", None, QtGui.QApplication.UnicodeUTF8))
        self.progress_label.setText(QtGui.QApplication.translate("ImportCtrDialogBase", "Pronto", None, QtGui.QApplication.UnicodeUTF8))

import resources_rc
