# -*- coding: utf-8 -*-
"""
/***************************************************************************
 ImportCtrDialog
                                 A QGIS plugin
 Script per l'importazione della CTRN Veneto e vestizione
                             -------------------
        begin                : 2015-03-31
        git sha              : $Format:%H$
        copyright            : (C) 2015 by Amedeo Fadini Tepco srl
        email                : tepco@tepco.it
 ***************************************************************************/

/***************************************************************************
 *                                                                         *
 *   This program is free software; you can redistribute it and/or modify  *
 *   it under the terms of the GNU General Public License as published by  *
 *   the Free Software Foundation; either version 2 of the License, or     *
 *   (at your option) any later version.                                   *
 *                                                                         *
 ***************************************************************************/
Code examples were taken from remotedebug plugin made by sourcepole.ch 
and mapsheetdownload plugin  by Casey Vandenberg / SJ Geophysics
"""

import os, time, subprocess
from PyQt4 import QtGui, uic
from PyQt4.QtCore import Qt, pyqtSlot
from PyQt4.QtGui import QFileDialog
from string import join

from qgis.core import *
from qgis.gui import *



#useful imports
import zipfile
import shutil
from _ast import arguments

#default values
file_src_path = ""
file_dst_path = ""

#enviroment path
plugin_dir = os.path.dirname(__file__)
temp_dir = os.path.join(plugin_dir, 'tmp')
data_dir = os.path.join(plugin_dir, 'data')

FORM_CLASS, _ = uic.loadUiType(os.path.join(
    os.path.dirname(__file__), 'ctr_import_dialog_base.ui'))

class ImportCtrDialog(QtGui.QDialog, FORM_CLASS):
    def __init__(self, parent=None):
        
        """Constructor."""
        super(ImportCtrDialog, self).__init__(parent)
        # Set up the user interface from Designer.
        # After setupUI you can access any designer object by doing
        # self.<objectname>, and you can use autoconnect slots - see
        # http://qt-project.org/doc/qt-4.8/designer-using-a-ui-file.html
        # #widgets-and-dialogs-with-auto-connect
        self.setupUi(self)
        #default values
        self.src_ledit.setText(file_src_path)
        self.dst_ledit.setText(file_dst_path)
        self.progressBar.setProperty("value", 0)        


    @pyqtSlot()
    def on_pushButtonSrc_clicked(self):
        print "sfoglia sorgente"
        file_src_path = QFileDialog.getExistingDirectory(
            None, "Seleziona la directory con i file sorgente",
            self.src_ledit.text())
        if not file_src_path:
            return  # dialog canceled
        self.src_ledit.setText(file_src_path)
        src_path = self.src_ledit.text()
        print src_path
        
    @pyqtSlot()    
    def on_pushButtonDst_clicked(self):
        print "sfoglia destinazione"
        file_dst_path = QFileDialog.getSaveFileName(
            None, "Seleziona il percorso e il nome del database di destinazione",
            self.src_ledit.text(), filter=self.tr("Database spatialite: (*.sqlite)",))
        if not file_dst_path:
            return  # dialog canceled
        self.dst_ledit.setText(file_dst_path)
        dst_path = self.dst_ledit.text()
        print dst_path
        
    @pyqtSlot()  
    def accept(self):
        print "accepted"
        file_src_path = self.src_ledit.text()
        #create db 
        db_name = self.dst_ledit.text()
        self.create_db(db_name)
        self.decomprimi(file_src_path)
        pass
        return



    
    
    def decomprimi(self, zippath):
       
       
        #decomprime ciascun file e lo salva in tmp
        temp_path = temp_dir
        totali = len(self.getZipList(zippath))
        i = 0        
        for file in self.getZipList(zippath):
            i += 1
            if (os.path.exists(temp_path)):
                shutil.rmtree(temp_path)
            print "crea tmp"
            os.mkdir(temp_path)
            zip = os.path.join(zippath, file)
            self.progress_label.setText("estraggo " + file + "; archivio "+ str(i) + " di " + str(totali))
            time.sleep(.02)

            with zipfile.ZipFile(zip, "r") as z:
                print zip
                z.extractall(temp_path +'/')
                db_name = self.dst_ledit.text()
                self.importa_shp(temp_path, db_name)
            
                           
        self.progress_label.setText(str(i) + " di " + str(totali) + "archivi processati.")
        time.sleep(.02)

        question = "Import completato. vuoi caricare i nuovi livelli i Qgis?"
        response = QtGui.QMessageBox.question(self, "Ctr importata", question, QtGui.QMessageBox.Yes | QtGui.QMessageBox.No)
        if response == QtGui.QMessageBox.Yes:
            print "yes"
        self.reject()
        
    def importa_shp(self, filedir, database):
        #needs existing database
        list = self.getShpListTree(filedir)
        progressBarRange = len(list)
        self.progressBar.setProperty("value", 0)
        self.progressBar.setRange(0,progressBarRange)            
        for filename in list:
            self.update_progress_bar()
            time.sleep(.02) 
            print filename
            #shapefile name
            layerfilename = os.path.basename(filename)
            #filename without extension
            layername = os.path.splitext(layerfilename)[0]
            #call ogr2ogr
            layername = layername.lower()
            arguments = ["ogr2ogr", "-append", database, filename, "-nln", layername]
            print 'shape ' + filename + ' nel db ' + database + ' tabella ' + layername
            print arguments
            startupinfo = None
            if os.name == 'nt':
                si = subprocess.STARTUPINFO()
                si.dwFlags |= subprocess.STARTF_USESHOWWINDOW
            subprocess.call(arguments, startupinfo = si) 
        
        

        
    def create_db(self, db_name):
        db_template = os.path.join(data_dir, "template_ctrn.zip")
        
        
        with zipfile.ZipFile(db_template, "r") as z:
            z.extract("template_ctrn.sqlite", os.path.dirname(db_name))
        #create new  database 
        if (os.path.exists(db_name)):
            print "rimuovendo il vecchio db"
            os.remove(db_name)
        os.rename(os.path.dirname(db_name)+"/template_ctrn.sqlite", db_name)

    def carica_db(selfself, db_name):
        pass
                
    def getShpListTree(self, init_path):
        """
        cerca gli shp in tutte le sottodirectory
        """
        shpHeadList = []    
        shpList = []
        for (path, dirs, files) in os.walk(init_path):
           
            shp_tot = len(files)
            print "shp tot:" + str(shp_tot)
            for fileName in files:
                if fileName.endswith('.shp'):
                    
                    shpList.append(str(os.path.join(path,fileName)))
        return shpList   

               
    """
    From MapsheetDownload by Casey Vandenberg / SJ Geophysics 
    """
    def update_progress_bar(self):
        """Update the progress bar."""
        self.progressBar.setValue(self.progressBar.value() + 1)
        time.sleep(.02)
            
    def getZipList(self, Dir):
        """
        Returns a list of zipfiles in the cwd
        """
        zipHeadList = []    
        zipList = []
        for fileName in os.listdir(Dir):
            fileHead = os.path.splitext(fileName)[0]
            fileTail = os.path.splitext(fileName)[1]
            if fileName.endswith('.zip'):
                zipHeadList.append(str(fileHead))
                zipList.append(str(fileName))
        return zipList

    def getShpList(self, Dir):
        """
        Returns a list of shapefiles in the cwd
        """
        shpHeadList = []    
        shpList = []
        for fileName in os.listdir(Dir):
            fileHead = os.path.splitext(fileName)[0]
            fileTail = os.path.splitext(fileName)[1]
            if fileName.endswith('.shp'):
                shpHeadList.append(str(fileHead))
                shpList.append(str(fileName))
        return shpList,shpHeadList